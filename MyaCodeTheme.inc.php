<?php
import('lib.pkp.classes.plugins.ThemePlugin');
class MyaCodeTheme extends ThemePlugin{
    public function init(){
        $this->addStyle('stylesheet','styles/index.less');
    }

    function getDisplayName()
    {
     return __('plugins.themes.mya-code-theme.name');
    }

    function getDescription()
    {
        return __('plugins.themes.mya-code-theme.description');;
    }
}